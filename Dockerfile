# Based on dockerhub fellah/gitbook
#
# We keep our own in gitlab-registry.cern.ch/cloud, just in case.

FROM node:6-slim

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

LABEL version=4.0.0-alpha.6

RUN npm install -g gitbook-cli && \
	gitbook fetch ${VERSION} && \
	npm cache clear && \
	rm -rf /tmp/*

WORKDIR /srv/gitbook

VOLUME /srv/gitbook /srv/html

EXPOSE 4000 35729

CMD /usr/local/bin/gitbook serve
